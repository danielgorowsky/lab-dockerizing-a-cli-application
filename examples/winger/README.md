oclif-hello-world
=================

oclif example Hello World CLI

[![oclif](https://img.shields.io/badge/cli-oclif-brightgreen.svg)](https://oclif.io)
[![Version](https://img.shields.io/npm/v/oclif-hello-world.svg)](https://npmjs.org/package/oclif-hello-world)
[![CircleCI](https://circleci.com/gh/oclif/hello-world/tree/main.svg?style=shield)](https://circleci.com/gh/oclif/hello-world/tree/main)
[![Downloads/week](https://img.shields.io/npm/dw/oclif-hello-world.svg)](https://npmjs.org/package/oclif-hello-world)
[![License](https://img.shields.io/npm/l/oclif-hello-world.svg)](https://github.com/oclif/hello-world/blob/main/package.json)

<!-- toc -->
* [Usage](#usage)
* [Commands](#commands)
<!-- tocstop -->
# Usage
<!-- usage -->
```sh-session
$ npm install -g winger
$ winger COMMAND
running command...
$ winger (--version)
winger/0.0.0 darwin-arm64 node-v19.8.1
$ winger --help [COMMAND]
USAGE
  $ winger COMMAND
...
```
<!-- usagestop -->
# Commands
<!-- commands -->
* [`winger hello PERSON`](#winger-hello-person)
* [`winger hello world`](#winger-hello-world)
* [`winger help [COMMANDS]`](#winger-help-commands)
* [`winger plugins`](#winger-plugins)
* [`winger plugins:install PLUGIN...`](#winger-pluginsinstall-plugin)
* [`winger plugins:inspect PLUGIN...`](#winger-pluginsinspect-plugin)
* [`winger plugins:install PLUGIN...`](#winger-pluginsinstall-plugin-1)
* [`winger plugins:link PLUGIN`](#winger-pluginslink-plugin)
* [`winger plugins:uninstall PLUGIN...`](#winger-pluginsuninstall-plugin)
* [`winger plugins:uninstall PLUGIN...`](#winger-pluginsuninstall-plugin-1)
* [`winger plugins:uninstall PLUGIN...`](#winger-pluginsuninstall-plugin-2)
* [`winger plugins update`](#winger-plugins-update)

## `winger hello PERSON`

Say hello

```
USAGE
  $ winger hello PERSON -f <value>

ARGUMENTS
  PERSON  Person to say hello to

FLAGS
  -f, --from=<value>  (required) Who is saying hello

DESCRIPTION
  Say hello

EXAMPLES
  $ oex hello friend --from oclif
  hello friend from oclif! (./src/commands/hello/index.ts)
```

_See code: [dist/commands/hello/index.ts](https://github.com/TylerGarlick/winger/blob/v0.0.0/dist/commands/hello/index.ts)_

## `winger hello world`

Say hello world

```
USAGE
  $ winger hello world

DESCRIPTION
  Say hello world

EXAMPLES
  $ winger hello world
  hello world! (./src/commands/hello/world.ts)
```

## `winger help [COMMANDS]`

Display help for winger.

```
USAGE
  $ winger help [COMMANDS] [-n]

ARGUMENTS
  COMMANDS  Command to show help for.

FLAGS
  -n, --nested-commands  Include all nested commands in the output.

DESCRIPTION
  Display help for winger.
```

_See code: [@oclif/plugin-help](https://github.com/oclif/plugin-help/blob/v5.2.8/src/commands/help.ts)_

## `winger plugins`

List installed plugins.

```
USAGE
  $ winger plugins [--core]

FLAGS
  --core  Show core plugins.

DESCRIPTION
  List installed plugins.

EXAMPLES
  $ winger plugins
```

_See code: [@oclif/plugin-plugins](https://github.com/oclif/plugin-plugins/blob/v2.4.3/src/commands/plugins/index.ts)_

## `winger plugins:install PLUGIN...`

Installs a plugin into the CLI.

```
USAGE
  $ winger plugins:install PLUGIN...

ARGUMENTS
  PLUGIN  Plugin to install.

FLAGS
  -f, --force    Run yarn install with force flag.
  -h, --help     Show CLI help.
  -v, --verbose

DESCRIPTION
  Installs a plugin into the CLI.
  Can be installed from npm or a git url.

  Installation of a user-installed plugin will override a core plugin.

  e.g. If you have a core plugin that has a 'hello' command, installing a user-installed plugin with a 'hello' command
  will override the core plugin implementation. This is useful if a user needs to update core plugin functionality in
  the CLI without the need to patch and update the whole CLI.


ALIASES
  $ winger plugins add

EXAMPLES
  $ winger plugins:install myplugin 

  $ winger plugins:install https://github.com/someuser/someplugin

  $ winger plugins:install someuser/someplugin
```

## `winger plugins:inspect PLUGIN...`

Displays installation properties of a plugin.

```
USAGE
  $ winger plugins:inspect PLUGIN...

ARGUMENTS
  PLUGIN  [default: .] Plugin to inspect.

FLAGS
  -h, --help     Show CLI help.
  -v, --verbose

GLOBAL FLAGS
  --json  Format output as json.

DESCRIPTION
  Displays installation properties of a plugin.

EXAMPLES
  $ winger plugins:inspect myplugin
```

## `winger plugins:install PLUGIN...`

Installs a plugin into the CLI.

```
USAGE
  $ winger plugins:install PLUGIN...

ARGUMENTS
  PLUGIN  Plugin to install.

FLAGS
  -f, --force    Run yarn install with force flag.
  -h, --help     Show CLI help.
  -v, --verbose

DESCRIPTION
  Installs a plugin into the CLI.
  Can be installed from npm or a git url.

  Installation of a user-installed plugin will override a core plugin.

  e.g. If you have a core plugin that has a 'hello' command, installing a user-installed plugin with a 'hello' command
  will override the core plugin implementation. This is useful if a user needs to update core plugin functionality in
  the CLI without the need to patch and update the whole CLI.


ALIASES
  $ winger plugins add

EXAMPLES
  $ winger plugins:install myplugin 

  $ winger plugins:install https://github.com/someuser/someplugin

  $ winger plugins:install someuser/someplugin
```

## `winger plugins:link PLUGIN`

Links a plugin into the CLI for development.

```
USAGE
  $ winger plugins:link PLUGIN

ARGUMENTS
  PATH  [default: .] path to plugin

FLAGS
  -h, --help     Show CLI help.
  -v, --verbose

DESCRIPTION
  Links a plugin into the CLI for development.
  Installation of a linked plugin will override a user-installed or core plugin.

  e.g. If you have a user-installed or core plugin that has a 'hello' command, installing a linked plugin with a 'hello'
  command will override the user-installed or core plugin implementation. This is useful for development work.


EXAMPLES
  $ winger plugins:link myplugin
```

## `winger plugins:uninstall PLUGIN...`

Removes a plugin from the CLI.

```
USAGE
  $ winger plugins:uninstall PLUGIN...

ARGUMENTS
  PLUGIN  plugin to uninstall

FLAGS
  -h, --help     Show CLI help.
  -v, --verbose

DESCRIPTION
  Removes a plugin from the CLI.

ALIASES
  $ winger plugins unlink
  $ winger plugins remove
```

## `winger plugins:uninstall PLUGIN...`

Removes a plugin from the CLI.

```
USAGE
  $ winger plugins:uninstall PLUGIN...

ARGUMENTS
  PLUGIN  plugin to uninstall

FLAGS
  -h, --help     Show CLI help.
  -v, --verbose

DESCRIPTION
  Removes a plugin from the CLI.

ALIASES
  $ winger plugins unlink
  $ winger plugins remove
```

## `winger plugins:uninstall PLUGIN...`

Removes a plugin from the CLI.

```
USAGE
  $ winger plugins:uninstall PLUGIN...

ARGUMENTS
  PLUGIN  plugin to uninstall

FLAGS
  -h, --help     Show CLI help.
  -v, --verbose

DESCRIPTION
  Removes a plugin from the CLI.

ALIASES
  $ winger plugins unlink
  $ winger plugins remove
```

## `winger plugins update`

Update installed plugins.

```
USAGE
  $ winger plugins update [-h] [-v]

FLAGS
  -h, --help     Show CLI help.
  -v, --verbose

DESCRIPTION
  Update installed plugins.
```
<!-- commandsstop -->
